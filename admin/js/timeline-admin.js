jQuery(document).ready(function($) {
	$('.timeline-entry-datepicker').datepicker({
		dateFormat : 'yy-mm-dd',
		timeFormat : 'hh:mm tt',
		changeMonth: true,
		changeYear: true,
		altField: '#alternate',
		altFormat: 'dd M yy',
		showAnim: 'slideDown',
		yearRange: '1800:2037',
		autoSize: true,
		// showButtonPanel: true,
		showOn: "button",
		buttonText: "<span class=\"dashicons dashicons-calendar-alt\"></span>"
	});
});
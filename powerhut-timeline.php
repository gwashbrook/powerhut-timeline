<?php
/*
Plugin Name: Powerhut Timelines
Description: Plugin description
Plugin URI: http://my.powerhut.net/
Author: Graham Washbrook
Author URI: http://powerhut.tel
Text Domain: plugin-text-domain
Domain Path: /languages
Version: 0.0.21
*/

//* Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

//* Define plugin path
define( 'PHUT_TIMELINE_PATH', plugin_dir_path( __FILE__ ) );

//* Includes and requires
require( PHUT_TIMELINE_PATH . 'config.php');

//* On plugin activation
function phut_timeline_activation() {
	if ( ! current_user_can( 'activate_plugins' ) )  return;
	phut_timeline_init();
	flush_rewrite_rules();
}
register_activation_hook( __FILE__, 'phut_timeline_activation' );

//* On plugin deactivation
function phut_timeline_deactivation() {
	// unregister_post_type('phut_timeline_entry'); // ?
	flush_rewrite_rules();
}
register_deactivation_hook( __FILE__, 'phut_timeline_deactivation' );

//* On plugin uninstall
function phut_timeline_uninstall() {
	/*
	// e.g. delete options
	$option_name = 'phut_timeline_option';
 	delete_option($option_name);
 
	// for site options in Multisite
	delete_site_option($option_name);
 
	// eg. drop a custom database table
	global $wpdb;
	$wpdb->query("DROP TABLE IF EXISTS {$wpdb->prefix}mytable");
	*/
	
	// unregister_post_type('phut_timeline_entry'); // ?
	// flush_rewrite_rules(); // ?
}
register_uninstall_hook( __FILE__, 'phut_timeline_uninstall');


//* Init
add_action( 'init', 'phut_timeline_init', 0 );
function phut_timeline_init() {

	// Register taxonomies
	phut_timeline_register_taxonomies();

	// Register post types
	phut_timeline_register_cpt();

	// Better be safe than sorry when registering custom taxonomies for custom post types.
	register_taxonomy_for_object_type( 'phut_timeline', 'phut_timeline_entry' );

	// Change enter title here text
	add_filter( 'enter_title_here', 'phut_timeline_enter_title_here' );

	// Change the post updated messages
	add_filter( 'post_updated_messages', 'phut_timeline_updated_messages' );

	// Change the bulk post updated messages
	add_filter( 'bulk_post_updated_messages', 'phut_timeline_bulk_updated_messages' );

	// Add image size
	// add_image_size( 'phut-timeline-entry-archive', 370, 370, array( 'left', 'top' ) );
	
	// add_image_size( 'phut-timeline-entry-archive', 256, 144, array( 'left', 'top' ) ); // 16:9
	
	add_image_size ( 'phut-timeline-entry-archive', 256, 144 );
	

} //fn




//* Register taxonomies
function phut_timeline_register_taxonomies() {

	// TAG
	
	$labels = array(

		'name'          => _x( 'Timelines', 'taxonomy general name' ),
		'singular_name' => _x( 'Timeline', 'taxonomy singular name' ),
		'menu_name'     => _x( 'Timelines', 'taxonomy general name' ),
		'all_items'     => __( 'All Timelines', 'plugin-text-domain' ),
		'edit_item'     => __( 'Edit Timeline', 'plugin-text-domain' ),
		'view_item'     => __( 'View Timeline', 'plugin-text-domain' ),
		'update_item'   => __( 'Update Timeline', 'plugin-text-domain' ),
		'add_new_item'  => __( 'Add New Timeline', 'plugin-text-domain' ),
		'new_item_name' => __( 'New Timeline', 'plugin-text-domain' ),
		'search_items'  => __( 'Search Timelines', 'plugin-text-domain' ),
		'popular_items' => __( 'Popular Timelines', 'plugin-text-domain' ),
		'not_found'     => __( 'No timelines found', 'plugin-text-domain' ),

		// FOR NON HIERARCHY ( tags )
		'separate_items_with_commas' => __( 'Separate timelines with commas', 'plugin-text-domain' ),
		'add_or_remove_items'        => __( 'Add or remove timelines', 'plugin-text-domain' ),
		'choose_from_most_used'      => __( 'Choose from the most used timelines', 'plugin-text-domain' ),

		// FOR HIERARCHY ( categories )
		// 'parent_item'       => __( 'Parent Timeline', 'plugin-text-domain' ),
		// 'parent_item_colon' => __( 'Parent Timeline:', 'plugin-text-domain' ), // Same as parent_item, but with a colon :

	);
	
	$rewrite = array(
		'slug'         => 'timeline',  // Used as pretty permalink text (i.e. /tag/) - defaults to $taxonomy (taxonomy's name slug)
		'with_front'   => true,         // allowing permalinks to be prepended with front base - defaults to true
		'hierarchical' => false,         // Boolean. Allow hierarchical urls (implemented in Version 3.1)  - default false	
	);
	
	$capabilities = array(
		'manage_terms' => 'manage_timelines',
		'edit_terms'   => 'edit_timelines',
		'delete_terms' => 'delete_timelines',
		'assign_terms' => 'assign_timelines'
	);
	
	$args = array(

		'labels'             => $labels,
		'public'             => true, // (boolean) optional ~ default true
			'publicly_queryable' => true,  // (boolean) optional ~ defaults to value of 'public'
			'show_in_nav_menus'  => true, // (boolean) optional ~ defaults to value of 'public'
			'show_ui'            => true,  // (boolean) optional ~ defaults to value of 'public'

		'show_in_menu'       => true, // (boolean) optional - here to show taxonomy in admin menu. 'show_ui' must be true ~ defaults to 'show_ui'
		'show_tagcloud'      => true, // (boolean) optional - Whether to allow the Tag Cloud widget to use this taxonomy ~ defaults to 'show_ui'
		'show_in_quick_edit' => true, // (boolean) optional ~ defaults to 'show_ui'
		
		

		
		
		// 'meta_box_cb'       => '', // (callback) optional ~ defaults to post_tags_meta_box() or post_categories_meta_box(). No metabox is shown if set to false
		'show_admin_column' => true, // (boolean) optional ~ default false
		'description'       => '', // (string) optional - Include a description of the taxonomy ~ default ''
		'hierarchical'      => false,  // (boolean) optional ~ default false
		'update_count_callback' => '_update_post_term_count', // To ensure taxonomy acts like a tag, set to '_update_post_term_count'
		'query_var'         => false, // false || string // default taxonomy name
		// 'capabilities'      => $capabilities,
		// 'sort' => true, // (boolean) optional - Whether should remember order that terms are added to objects ~ default None
		
		'rewrite'           => $rewrite,
		'delete_with_user'  => false,
		
		// 'show_in_rest'       => true, // (boolean) optional ~ default false
    	// 'rest_base'          => '*******', // (string) (optional) To change the base url of REST API route
		// 'rest_controller_class' =>


	);
	
	register_taxonomy( 'phut_timeline', array( 'phut_timeline_entry' ), $args );
	// register_taxonomy( 'phut_month', array( 'phut_ingredient','phut_recipe' ), $args );


	// TODO Comment out when in production
	// flush_rewrite_rules( false );

} //fn



//* Register post types
function phut_timeline_register_cpt() {

	$supports = array (
		// 'title',
		'excerpt',
		'editor',
		'author',
		'thumbnail',
		'custom_fields',
		// 'comments',
		// 'revisions',
		// 'genesis_seo',
		'genesis-layouts',
		// 'genesis-simple-sidebars',
		'genesis-scripts',
		'genesis-cpt-archives-settings',
		'genesis-entry-meta-after-content',
		// 'autodescription-meta',             // The SEO Framework
		// 'page-attributes',                  // menu order ( hierarchical post types only )
	);

	$labels = array (
		'name'                  => 'Timeline Entries', // Seen in : Breadcrumbs, All Items list
		'singular_name'         => 'Timeline Entry',
		'menu_name'             => 'Timeline Entries',
		'name_admin_bar'        => 'Timeline Entry',
		'all_items'             => __( 'All Timeline Entries', 'plugin-text-domain' ),
		'add_new'               => __( 'Add New', 'plugin-text-domain' ),
	 	'add_new_item'          => __( 'Add a new Timeline Entry', 'plugin-text-domain' ),
		'edit_item'             => __( 'Edit Timeline Entry', 'plugin-text-domain' ),
		'new_item'              => __( 'New Timeline Entry', 'plugin-text-domain' ),
		'view_item'             => __( 'View Timeline Entry', 'plugin-text-domain' ),
		'search_items'          => __( 'Search Timeline Entries', 'plugin-text-domain' ),
		'not_found'             => __( 'No Timeline Entries found', 'plugin-text-domain' ),
		'not_found_in_trash'    => __( 'No Timeline Entries found in Bin', 'plugin-text-domain' ),
		// 'parent_item_colon'  => __( 'Parent Timeline Entry', 'plugin-text-domain' ), // only used in hierarchical post types
		'archives'              => __( 'Timeline Entry Archives', 'plugin-text-domain' ), // for use with archives in nav menus. Default is Post Archives/Page Archives.
		'insert_into_item'      => __( 'Insert into Timeline Entry', 'plugin-text-domain' ), // for the media frame button. Default is Insert into post/Insert into page.
		'uploaded_to_this_item' => __( 'Uploaded to this Timeline Entry', 'plugin-text-domain' ), // for the media frame filter. Default is Uploaded to this post/Uploaded to this page.
		'attributes'            => __( 'Timeline Entry Attributes', 'plugin-text-domain' ), // for the attributes meta box. Default is 'Post Attributes' / 'Page Attributes'.
		'featured_image'        => __( 'Featured Image', 'plugin-text-domain' ), // Default is Featured Image.
		'set_featured_image'    => __( 'Set featured image', 'plugin-text-domain' ), // Default is Set featured image.
		'remove_featured_image' => __( 'Remove featured image', 'plugin-text-domain' ), // Default is Remove featured image.
		'use_featured_image'    => __( 'Use as featured image', 'plugin-text-domain' ), // Default is Use as featured image. 		
		
		
	);
	
	$rewrite = array (
		// 'slug'              => 'timeline-entries',
		'slug'              => 'timeline-entry',
		
		'with_front'        => false, // eg: if permalink structure is /blog/, then links will be: false->/news/, true->/blog/news/). Defaults to true
		'feeds'             => false, // Defaults to has_archive value
		'pages'             => true, // Should the permalink structure provide for pagination. Defaults to true
	);
	
	$taxonomies = array (
		'phut_timeline',
	);
	
	$args = array (
	
		'label'                => 'Timeline Entry', // Shown in breadcrumbs, archive title, admin list table, menu items
		'labels'               => $labels,
		'description'          => '', // (string) (optional) A short descriptive summary of what the post type is. May be visible, e.g. in menus
		'public'               => true,  // true || false - optional : Needs to be true for Yoast
			'exclude_from_search' => false, // needs to be false if want to see cpts on cpt-archive page. If need to exclude from search, then filter the search query
			'publicly_queryable'  => true,
			'show_in_nav_menus'   => false,
			// 'show_in_nav_menus'   => true,
			'show_ui'             => true,
		
		'show_in_menu'         => true,
		'show_in_admin_bar'    => true,
		// 'menu_position'        => NULL, // Defaults to below Comments
		'menu_icon'            => 'dashicons-clock',
		'capability_type'      => 'post', // or 'phut_timeline_entry' referenced
		'map_meta_cap'         => true,
		'hierarchical'         => false,
		'supports'             => $supports,
		
		// Called when setting up the meta boxes for the edit form.
		// Takes one argument $post, which contains the WP_Post object for the currently edited post.
		// Do remove_meta_box() and add_meta_box() calls in the callback.
		'register_meta_box_cb' => 'phut_timeline_meta_box_cb',
		
		'taxonomies'           => $taxonomies,
		'has_archive'          => true,
		'has_archive'          => false,
		'rewrite'              => $rewrite,		
		'query_var'            => true, // defaults to true (custom post type name)
		'can_export'           => true, // default true
		
		// 'show_in_rest'         => true, // default false
		// 'rest_base'            => 'restbase',
		

		
	);
	
	register_post_type( 'phut_timeline_entry', $args );
	
	
	// Comment or remove following flush rewrite rules when not in development !!!
	// flush_rewrite_rules();

}

//* Change enter title here text
function phut_timeline_enter_title_here( $title ) {

	$screen = get_current_screen();
	if( $screen->post_type == 'phut_timeline_entry' )
		$title = 'Enter title here';

	return $title;
}

//* Change the post updated messages
function phut_timeline_updated_messages( $messages ) {

	global $post;

	// Add messages to $messages array
	$messages['phut_timeline_entry'] = array(
		0  => '', // Unused. Messages start at index 1.
		1  => sprintf( 'Timeline Entry updated. <a href="%s">View Timeline Entry</a>', esc_url( get_permalink($post->ID) ) ),
		2  => 'Custom field updated.',
		3  => 'Custom field deleted.',
		4  => 'Timeline Entry updated.',
		5  => isset( $_GET['revision']) ? sprintf( 'Timeline Entry restored to revision from %s', wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
		6  => sprintf( 'Timeline Entry published. <a href="%s">View Timeline Entry</a>', esc_url( get_permalink( $post->ID ) ) ),
		7  => 'Timeline Entry saved.',
		8  => sprintf( 'Timeline Entry submitted. <a target="_blank" href="%s">Preview Timeline Entry</a>', esc_url( add_query_arg( 'preview', 'true', get_permalink( $post->ID ) ) ) ),
		9  => sprintf( 'Timeline Entry publishing scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview Timeline Entry</a>', date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ), esc_url( get_permalink( $post->ID ) ) ),
		10 => sprintf( 'Timeline Entry draft updated. <a target="_blank" href="%s">Preview Timeline Entry</a>', esc_url( add_query_arg( 'preview', 'true', get_permalink( $post->ID ) ) ) ),
	);

	return $messages;
}

//* Change the bulk post updated messages
function phut_timeline_bulk_updated_messages( $bulk_messages ) {

	global $bulk_counts;
	
	// Add messages to $bulk_messages array
	$bulk_messages['phut_timeline_entry'] = array(
		'updated'   => _n( '%s Timeline Entry updated.', '%s Timeline Entries updated.', $bulk_counts['updated'] ),
		'locked'    => _n( '%s Timeline Entry not updated, somebody is editing it.', '%s Timeline Entries not updated, somebody is editing them.', $bulk_counts['locked'] ),
		'deleted'   => _n( '%s Timeline Entry permanently deleted.', '%s Timeline Entries permanently deleted.', $bulk_counts['deleted'] ),
		'trashed'   => _n( '%s Timeline Entry moved to Bin.', '%s Timeline Entries moved to Bin.', $bulk_counts['trashed'] ),	
		'untrashed' => _n( '%s Timeline Entry restored from Bin.', '%s Timeline Entries restored from Bin.', $bulk_counts['untrashed'] ),
	);

	return $bulk_messages;

}


//* Preferably, I would like this above the editor
function phut_timeline_meta_box_cb() {

	add_meta_box(
		'timeline-entry-info', // string
		'Timeline Entry Info', // string
		'phut_timeline_render_mb', // callback
		'phut_timeline_entry', // string|array|WP_Screen $screen = null
		'advanced', // string $context = 'advanced'
		'high', // string $priority = 'default'
		null // array $callback_args = null
	);

}

//*
function phut_timeline_render_mb( $post ) {

	wp_nonce_field( basename( __FILE__ ), 'timeline_entry_mb_nonce' );
	
	/*
	$current_meta = get_post_meta( $post->ID, '_phut_timeline_meta', true );
	// Check for empty string or array
	if( $current_meta != '' ) {
		if( isset( $current_meta['entry_dt'] ) ) $current_entry_dt = $current_meta['entry_dt'];
	}
	*/
	
	$current_entry_dt = get_post_meta( $post->ID, '_phut_timeline_entry_dt', true );
	
	?>
	<table class="form-table">
	<tbody>
	<tr valign="top">
	<th scope="row">Date</th>
	<td><label for="entry_dt" class="screen-reader-text">Date</label><input readonly type="text" id="alternate" /><input type="text" name="entry_dt" class="medium-text timeline-entry-datepicker" value="<?php echo $current_entry_dt ?>" /><br /><span class="description">Select a date for this entry.</span></td>
	</tr>
	</tbody>
	</table>
	<?php
}


// Set the post title and slug to be something related to the entry date / time / postid / something else
add_action('save_post_phut_timeline_entry', 'my_function');
function my_function( $post_id ){

	// Check nonce
	if ( !isset( $_POST['timeline_entry_mb_nonce'] ) || !wp_verify_nonce( $_POST['timeline_entry_mb_nonce'], basename( __FILE__ ) ) )
		return $post_id;

	// Return for autosave
	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE )
		return $post_id;
		
	// Check the user's permissions.
	if ( ! current_user_can( 'edit_post', $post_id ) )
		return $post_id;
	
	
	// $datetime = DateTime::createFromFormat("D F d, Y H:i a", 'Wed January 9, 2013 01:43 pm');
	// echo $datetime->format("Y-m-d H:i:s");
	
	/*
	$new_timeline_meta = array(
		'entry_dt' => sanitize_text_field( $_POST['entry_dt'] ),
	);
	update_post_meta( $post_id, '_phut_timeline_meta', $new_timeline_meta );
	*/
	update_post_meta( $post_id, '_phut_timeline_entry_dt', sanitize_text_field( $_POST['entry_dt'] ) );


	if ( ! wp_is_post_revision( $post_id ) ){
	
		// unhook this function so it doesn't loop infinitely
		remove_action('save_post_phut_timeline_entry', 'my_function');
		
		$post_title = sanitize_text_field( $_POST['entry_dt'] );
		$post_slug = $post_id . ' - ' . sanitize_title( $post_title );
	
		$my_args = array(
			'ID' => $post_id,
			'post_title' => $post_title,
			'post_name'  => $post_slug,
		);
	
		// update the post, which calls save_post again
		wp_update_post( $my_args );

		// re-hook this function
		add_action('save_post_phut_timeline_entry', 'my_function');
	}

	
	return $post_id;
}




// Seletively enqueue admin styles / admin scripts
add_action( 'admin_enqueue_scripts', 'phut_timeline_enqueue_admin');
function phut_timeline_enqueue_admin( $hook_suffix ){

    $cpt = 'phut_timeline_entry';

    if( in_array($hook_suffix, array('post.php', 'post-new.php') ) ){
	
        $screen = get_current_screen();

        if( is_object( $screen ) && $cpt == $screen->post_type ){

            // Register, enqueue scripts and styles here
			wp_enqueue_script(
				'timeline-admin-js',
				plugin_dir_url( __FILE__ ) . 'admin/js/timeline-admin.js',
				array(
					'jquery-ui-datepicker',
				),
				null,
				true
			);

			wp_enqueue_style( 'jquery-ui-datepicker-style' , '//code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css');
			wp_enqueue_style( 'timeline-admin-style' , plugin_dir_url( __FILE__ ) . 'admin/css/timeline-admin.css');
        }
    }
}



add_action( 'wp_enqueue_scripts','phut_timeline_enqueue');
function phut_timeline_enqueue () {
	wp_enqueue_style( 'timeline-style' , plugin_dir_url( __FILE__ ) . 'public/css/timeline.css');

}



// Order timeline entries by custom field
add_filter('pre_get_posts', 'order_timeline_by_entry_dt');
function order_timeline_by_entry_dt( $query ) {

	// do not modify queries in the admin
	if( is_admin() ) return $query;
	
	if( 
		( is_post_type_archive( 'phut_timeline_entry' ) || is_tax( 'phut_timeline' ) )
		&&
		$query->is_main_query()
	) {
			$query->set( 'posts_per_page', PHUT_TIMELINE_ENTRIES_PER_PAGE );
			$query->set( 'orderby', 'meta_value' );
			$query->set( 'meta_key', '_phut_timeline_entry_dt' );
			$query->set( 'order', 'ASC' );
	}

	// Remove Archive Pagination if using load more?
	// remove_action( 'genesis_after_endwhile', 'genesis_posts_nav' );

	return $query;
}


add_action('genesis_meta','tasdasd');
function tasdasd() {
	
	if( is_post_type_archive( 'phut_timeline_entry' ) || is_tax('phut_timeline') ) {
		add_filter( 'genesis_attr_entry','myFn', 11);
		add_action('genesis_before_while','phut_open_timeline');
		add_action('genesis_after_endwhile','phut_close_timeline',9);
		
		remove_action( 'genesis_loop', 'genesis_do_loop' );
		add_action( 'genesis_loop', 'phut_timeline_loop');
		
		remove_action( 'genesis_entry_content', 'genesis_do_post_image', 8 );
		
		add_action( 'genesis_entry_footer', 'phut_timeline_footer');
		
	}




/*
	if( is_tax('phut_timeline') ) {
	
		add_action('genesis_before_loop','phut_timeline_term_cover');

	}
*/	
	
	
	
	
}

/*
function phut_timeline_term_cover() {

		$term_img_url = gtaxi_get_taxonomy_image( array(
			// Add arguments
			'size' => 'single-cover',
			'fallback' => '',
		));
		
		if ( $term_img_url ) {
			printf( '%s', $term_img_url );
		}

}
*/


function phut_timeline_footer() {
	
	echo '<p class="entry-meta">';
	echo get_the_term_list( get_the_ID(), 'phut_timeline', '<span class="entry-tags">', ' ', '</span>' );
	echo '</p>';

}



function phut_timeline_loop(){
	if ( have_posts() ) :
		do_action( 'genesis_before_while' );
		while ( have_posts() ) : the_post();
			do_action( 'genesis_before_entry' );
			phut_timeline_archive_entry();
			do_action( 'genesis_after_entry' );
		endwhile;
		do_action( 'genesis_after_endwhile' );
	else : //* if no entries
		do_action( 'genesis_loop_else' );
	endif;
}


function phut_timeline_archive_entry(){
	$current_entry_dt = get_post_meta( get_the_ID(), '_phut_timeline_entry_dt', true );
	$date = new DateTime( $current_entry_dt );
	printf( '<article %s>', genesis_attr( 'entry' ) );	?>
		<div class="timeline-icon"></div>
		<div class="timeline-content">
			<div class="timeline-date"><a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php echo $date->format('jS F Y');  ?></a></div>
			<?php
				do_action( 'genesis_before_entry_content' );
				printf( '<div %s>', genesis_attr( 'entry-content' ) );
				do_action( 'genesis_entry_content' );
				echo '</div>';
				do_action('genesis_after_entry_content');
				if ( has_post_thumbnail() ) : ?>
					<div class="timeline-image">
					<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_post_thumbnail('phut-timeline-entry-archive'); ?></a>
					</div>
				<?php endif; ?>
				
				<?php do_action('genesis_entry_footer'); ?>
	
		</div><!--// timeline-content-->
	</article>
<?php }






// Customise genesis_attr

function myFn( $attributes ){
	global $wp_query;

	if( is_post_type_archive( 'phut_timeline_entry' ) || is_tax('phut_timeline') ) {

		$attributes['class'] .= ($wp_query->current_post % 2 == 0) ? '' : ' alt';
	}
	return $attributes;
}


// Wrap loop in div#timeline
function phut_open_timeline(){

	/*
	$bgcolor =  get_term_meta( get_queried_object_id(), 'color', true);

	if( '' != $bgcolor) {
		printf( '<div class="timeline" style="background-color:#%s">', $bgcolor );
		
	} else {
		echo '<div class="timeline">';
	}
	*/
	
	echo '<div class="timeline">';
	
}


function phut_close_timeline(){
	echo '</div>';	
}



/* ADD OPTION TO TIMELINE TAXONOMY 
// See https://themehybrid.com/weblog/introduction-to-wordpress-term-meta

add_action( 'init', 'phut_timeline_register_term_meta' );

function phut_timeline_register_term_meta() {
    register_meta( 'term', 'color', 'phut_sanitize_hex' );
	// register_meta( 'term', 'url','esc_url_raw');
}


function phut_sanitize_hex( $color ) {
    $color = ltrim( $color, '#' );
    return preg_match( '/([A-Fa-f0-9]{3}){1,2}$/', $color ) ? $color : '';
}




function jt_get_term_color( $term_id, $hash = false ) {
    $color = get_term_meta( $term_id, 'color', true );
    $color = phut_sanitize_hex( $color );
    return $hash && $color ? "#{$color}" : $color;
}


//
add_action( 'phut_timeline_add_form_fields', 'ccp_new_term_field' );
function ccp_new_term_field() {

    wp_nonce_field( basename( __FILE__ ), 'phut_timeline_term_nonce' ); ?>

    <div class="form-field jt-term-color-wrap">
        <label for="jt-term-color"><?php _e( 'Background colour', 'jt' ); ?></label>
        <input type="text" name="jt_term_color" id="jt-term-color" value="" class="jt-color-field" data-default-color="#ffffff" />
    </div>
    
    <!--
    <div class="form-field">
		<label for="">URL</label>
		<input type="url" name="" id="" value="" class="" />
	</div>
  	-->
   
    
<?php }



//
add_action( 'phut_timeline_edit_form_fields', 'ccp_edit_term_color_field' );
function ccp_edit_term_color_field( $term ) {

    $default = '#ffffff';
    $color   = jt_get_term_color( $term->term_id, true );

    if ( ! $color )
        $color = $default; ?>

    <tr class="form-field jt-term-color-wrap">
        <th scope="row"><label for="jt-term-color"><?php _e( 'Background colour', 'jt' ); ?></label></th>
        <td>
            <?php wp_nonce_field( basename( __FILE__ ), 'phut_timeline_term_nonce' ); ?>
            <input type="text" name="jt_term_color" id="jt-term-color" value="<?php echo esc_attr( $color ); ?>" class="jt-color-field" data-default-color="<?php echo esc_attr( $default ); ?>" />
        </td>
    </tr>
<?php }


//
add_action( 'edit_phut_timeline',   'jt_save_term_color' );
add_action( 'create_phut_timeline', 'jt_save_term_color' );
function jt_save_term_color( $term_id ) {

    if ( ! isset( $_POST['phut_timeline_term_nonce'] ) || ! wp_verify_nonce( $_POST['phut_timeline_term_nonce'], basename( __FILE__ ) ) )
        return;

    $old_color = jt_get_term_color( $term_id );
    $new_color = isset( $_POST['jt_term_color'] ) ? phut_sanitize_hex( $_POST['jt_term_color'] ) : '';

    if ( $old_color && '' === $new_color )
        delete_term_meta( $term_id, 'color' );

    else if ( $old_color !== $new_color )
        update_term_meta( $term_id, 'color', $new_color );
}

//
add_filter( 'manage_edit-phut_timeline_columns', 'jt_edit_term_columns' );
function jt_edit_term_columns( $columns ) {

    $columns['color'] = __( 'Colour', 'jt' );

    return $columns;
}

//
add_filter( 'manage_phut_timeline_custom_column', 'jt_manage_term_custom_column', 10, 3 );
function jt_manage_term_custom_column( $out, $column, $term_id ) {

    if ( 'color' === $column ) {

        $color = jt_get_term_color( $term_id, true );

        if ( ! $color )
            $color = '#ffffff';

        $out = sprintf( '<span class="color-block" style="background:%s;">&nbsp;</span>', esc_attr( $color ) );
    }

    return $out;
}


//
add_action( 'admin_enqueue_scripts', 'jt_admin_enqueue_scripts' );
function jt_admin_enqueue_scripts( $hook_suffix ) {

	if( !in_array( $hook_suffix, array('edit-tags.php','term.php') ) || 'phut_timeline' !== get_current_screen()->taxonomy )    
        return;

    wp_enqueue_style( 'wp-color-picker' );
    wp_enqueue_script( 'wp-color-picker' );

    add_action( 'admin_head',   'jt_term_colors_print_styles' );
    add_action( 'admin_footer', 'jt_term_colors_print_scripts' );
}


//
function jt_term_colors_print_styles() { ?>

    <style type="text/css">
        .column-color { width: 50px; }
        .column-color .color-block { display: inline-block; width: 28px; height: 28px; border: 1px solid #ddd; }
    </style>
<?php }

//
function jt_term_colors_print_scripts() { ?>

    <script type="text/javascript">
        jQuery( document ).ready( function( $ ) {
            $( '.jt-color-field' ).wpColorPicker();
        } );
    </script>
<?php }
*/